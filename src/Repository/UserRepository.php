<?php

namespace App\Repository;

use App\Entity\User;
use App\Services\Helper;
use App\Services\TokenHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(User $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(User $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param $request
     * @return string[]
     */
    public function addNewUser($request, Helper $helper): array
    {
        $request = $helper->jsonToBody($request);
        $user = new User();
        $user->setLastName($request->get('last_name'))
            ->setName($request->get('name'))
            ->setMiddleName($request->get('middle_name'));
        $inputDate = $request->get('bd');
        $birthDate = (new \DateTimeImmutable($inputDate, null));
        $user->setBirthDate($birthDate)
            ->setLockFlag(false)
            ->setAccessToken(TokenHelper::createToken($request));
        $this->add($user, true);

        $data = [
            "message" => "Пользователь успешно создан",
        ];

        return $data;
    }

    /**
     * @param $token
     * @return bool
     */
    public function checkLock($token): bool
    {
        $query = $this->findOneBy([
            "accessToken" => $token,
            "lockFlag" => 0
        ]);
        if ($query != null) {
            return true;
        }
        else return false;
    }
}
