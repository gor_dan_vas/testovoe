<?php

namespace App\Services;

use App\Repository\UserRepository;

class TokenHelper
{
    /**
     * @param $request
     * @return string
     */
    public static function createToken($request): string
    {
        $lastName = $request->get('last_name');
        $name = $request->get('name');
        $middleName = $request->get('middle_name');
        $bd = $request->get('bd');

        $token = md5($lastName.$name.$middleName.$bd);

        return $token;
    }
}
