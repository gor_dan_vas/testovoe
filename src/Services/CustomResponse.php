<?php

namespace App\Services;

class CustomResponse
{
    public const HTTP_OK = 20000;
    public const INVALID_TOKEN = 30001;
    public const USER_BLOCKED = 30002;
    public const HTTP_NOT_FOUND = 40001;

}