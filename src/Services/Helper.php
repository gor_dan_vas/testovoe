<?php

namespace App\Services;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

class Helper
{

    /**
     * @param $code
     * @param $data
     * @param $requestId
     * @return array
     */
    public function getResponseForm($code, $data, $requestId): array
    {
        if ($code == 20000) {
            $array = [
                "request_id" => $requestId,
                "status" => 'success',
                "code" => $code,
                "error_message" => "",
                "data" => $data
            ];
            return $array;
        }
        elseif ($code == 40001) {
            $array = [
                "request_id" => $requestId,
                "status" => 'error',
                "code" => $code,
                "error_message" => "Вызываемый метод отсутствует"
            ];
            return $array;
        }
        else {
            $array = [
                "request_id" => $requestId,
                "status" => 'error',
                "code" => $code,
                "error_message" => "Ошибка проверки данных",
                "data" => $data
            ];
            return $array;
        }
    }

    /**
     * @param Request $request
     * @return Request
     */
    public function jsonToBody(Request $request): Request
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            return $request;
        }
        $request->request->replace($data);
        return $request;
    }
}
