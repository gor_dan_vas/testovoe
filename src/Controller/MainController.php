<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Services\CustomResponse;
use App\Services\Helper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/ping", name="ping", methods={"GET"})
     */
    public function ping(Request $request, Helper $helper): Response
    {
        $data = [
            "message" => "Сервис работает"
        ];
        $requestId = $request->headers->get('request_id');

        $data = $helper->getResponseForm(CustomResponse::HTTP_OK, $data, $requestId);
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @param Helper $helper
     * @return Response
     * @Route("/user/info", name="userInfo", methods={"GET"})
     */
    public function userInfo(Request $request, UserRepository $userRepository, Helper $helper): Response
    {
        $data = [];
        $requestId = $request->headers->get('request_id');

        $userInfo = $userRepository->findAll();
        foreach ($userInfo as $userData) {
            $data[] = [
                "fio" => $userData->getLastName()." ".$userData->getName()." ".$userData->getMiddleName(),
                "bd" => date ("Y-m-d H:i:s", $userData->getBirthDate()->getTimestamp()),
                "date" => date("Y-m-d H:i:s", $userData->getCreationDate()->getTimestamp()),
                "is_active" => !$userData->getLockFlag()
            ];
        }

        $data = $helper->getResponseForm(CustomResponse::HTTP_OK, $data, $requestId);
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @param Helper $helper
     * @return Response
     * @Route("user/create", name="userCreate", methods={"POST"})
     */
    public function userCreate(Request $request, UserRepository $userRepository, Helper $helper): Response
    {
        $requestId = $request->headers->get('request_id');
        $data = $userRepository->addNewUser($request, $helper);
        $data = $helper->getResponseForm(CustomResponse::HTTP_OK, $data, $requestId);
        return new JsonResponse($data, Response::HTTP_OK);
    }
}
