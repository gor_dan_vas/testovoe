<?php

namespace App\Controller;

use App\Services\CustomResponse;
use App\Services\Helper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ErrorController extends AbstractController
{
    /**
     * @Route("/error", name="app_error")
     */
    public function error(Request $request, Helper $helper): Response
    {
        $requestId = $request->headers->get('request_id');
        return new JsonResponse($helper->getResponseForm(CustomResponse::HTTP_NOT_FOUND, [], $requestId), Response::HTTP_OK);
    }
}
