<?php

namespace App\Security;

use App\Repository\UserRepository;
use App\Services\CustomResponse;
use App\Services\Helper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Uid\Uuid;

class ApiAuthenticator extends \Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request): ?bool
    {
        return true;
    }

    /**
     * @param Request $request
     * @return Passport
     */
    public function authenticate(Request $request): Passport
    {
        $apiToken = $request->headers->get('x-api-token');
        if (null === $apiToken) {
            throw new AuthenticationException("Пустой токен");
        }

        if (!$this->userRepository->checkLock($apiToken)) {
            throw new AuthenticationException("Пользователь заблокирован");
        }
        return new SelfValidatingPassport(new UserBadge($apiToken));
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $apiToken = $request->headers->get('x-api-token');
        $requestId = $request->headers->get('request_id');
        $code = CustomResponse::INVALID_TOKEN;
        $message = "Неверный токен";
        if ($this->userRepository->checkLock($apiToken)) {
            $message = "Пользователь заблокирован";
            $code = CustomResponse::USER_BLOCKED;
        }
        $data = [
            "message" => $message
        ];
        $data = (new Helper())->getResponseForm($code, $data, $requestId);
        return new JsonResponse($data, Response::HTTP_OK);
    }
}