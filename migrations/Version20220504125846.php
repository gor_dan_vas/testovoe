<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220504125846 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, last_name VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, middle_name VARCHAR(255) NOT NULL, birth_date DATE NOT NULL, lock_flag TINYINT(1) NOT NULL, access_token VARCHAR(255) NOT NULL, creation_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, changes_date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql("INSERT INTO `user` (`id`, `last_name`, `name`, `middle_name`, `birth_date`, `lock_flag`, `access_token`, `creation_date`, `changes_date`) VALUES (NULL, 'ADMIN', 'ADMIN', 'ADMIN', '1970-01-01', '0', '4412a75db2ca0a2247f291d68c3da19f', '2013-05-02', null)");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE `user`');
    }
}
